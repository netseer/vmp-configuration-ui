APP_MODULE
	.config([
		'$stateProvider',
		'config',
		function($stateProvider, config) {

			// ROUTES
			$stateProvider
				.state('app', {
					abstract: true,
					templateUrl: '/app/abstract/app/app.html',
					controller: 'AppCtrl'
				})
				.state('app.settings-form', {
					url: config.env === 'development' ? '/' : '/index.html',
					templateUrl: '/app/states/settings-form/settings-form.html',
					controller: 'PlacementQueueCtrl'
				})
		}
	]);
