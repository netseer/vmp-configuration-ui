APP_MODULE
	.config([
		'$locationProvider',
		'$stateProvider',
		'$urlRouterProvider',
		function($locationProvider, $stateProvider, $urlRouterProvider) {

			$locationProvider.html5Mode(true);

			// Catch all for unknown routes
			$urlRouterProvider.otherwise('/404');

			// public routes

			$stateProvider
				.state('public', {
					abstract: true,
					templateUrl: '/app/abstract/public/public.html',
					controller: 'PublicCtrl'
				})

				//.state('public.index', {
				//	url: '/',
				//	templateUrl: '/app/states/public/index/index.html',
				//	controller: 'IndexCtrl'
				//})

			// error routes
			$stateProvider
				.state('404', {
					url: '/404',
					templateUrl: '/app/states/shared/404/404.html',
					controller: '404Ctrl'
				});
		}
	]);
