var APP_MODULE = angular.module('app-framework',
	[
		'ui.router',
        'templates-app',
		'angular-util',
		'ui.bootstrap',
		'ngFileUpload'
	])

	.run([
		'$rootScope',
		'$state',
		'config',
		'Util',
		function ($rootScope, $state, config, Util) {

			$rootScope.$on('$stateChangeSuccess', function(ev, toState, toParams, fromState, fromParams) {

				if (toState.redirectTo) {
					ev.preventDefault();
					$state.go(toState.redirectTo, toParams)
				}

				hideLoading();

			});

			$rootScope.$on('$stateChangeError', function(ev, toState, toParams, fromState, fromParams, error) {

				hideLoading();
				Util.handleError(error);

			});

			$rootScope.$on('$stateChangeStart', showLoading);
			$rootScope.$on('showPageLoading', showLoading);
			$rootScope.$on('$stateChangeSuccess', hideLoading);
			$rootScope.$on('hidePageLoading', hideLoading);

			function showLoading() {
				$rootScope.showPageLoading = true;
			}

			function hideLoading() {
				$rootScope.showPageLoading = false;
			}

		}
	]);