APP_MODULE

  .directive('pageLoader', [function() {
    return {
      replace: true,
      template: '<div class="page-loading ng-cloak">' +
                  '<div class="filter"></div>' +
                  '<i class="fa fa-spinner fa-spin"></i>' +
                '</div>'
    }
  }]);