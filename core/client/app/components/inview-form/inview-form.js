APP_MODULE
	.directive('inviewForm', [function () {

		return {
			restrict: 'E',
			replace: true,
			controller: 'inviewFormCtrl',
			templateUrl: '/app/components/inview-form/inview-form.html',
			link: function(a, el, c) {


			}
		}

	}])
	.controller('inviewFormCtrl', [
		'$scope',
		'$state',
		function ($scope, $state) {

			// Options for various dropdowns in the UI.
			$scope.options = {
				alignment: [
					{label:"Left",value:"left"},
					{label:"Center",value:"center"},
					{label:"Right",value:"right"}
				]
			};

		}]);