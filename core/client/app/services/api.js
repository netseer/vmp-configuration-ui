APP_MODULE

	.factory('API', [
		'$q',
		'$http',
		'$timeout',
		'Util',
		'config',
		function($q, $http, $timeout, Util, config) {

			var apiHost = config.apiUrl;
			var appConfig = config || {};
			var cache = {};

			return {

				post: function(method, params, done) {

					var url = appConfig.env === 'development' ? "http://localhost:3000/app/lib/mock-data/settings-update-success.json" : apiHost + method;

					if (typeof params === 'function') {
						done = params;
						params = undefined;
					}

					if (typeof done !== 'function') {
						done = angular.noop();
					}

					var httpConfig = {
						withCredentials: true
					};

					var config = {
						headers : {
							'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
						}
					}

					var request = $http.post(url, params, config);

					return Util.responseHandler(request, done);

				},

				get: function(method, params, done) {

					var url = appConfig.env === 'development' ? "http://localhost:3000/app/lib/mock-data/settings-fetch-inimage.json" : apiHost + method;
					// var url = appConfig.env === 'development' ? "http://localhost:3000/app/lib/mock-data/settings-fetch-inview.json" : apiHost + method;
					var request = $http.get(url, { params: params });
					return Util.responseHandler(request, done);

				}

			};
		}
	]);

