APP_MODULE
    .factory('Settings', [
        'API',
        '$q',
        '$state',
        'config',
        function (API, $q, $state, config) {

            return {
                fetch: fetch,
                update: update
            };

            function fetch (params, done) {
                var method = config.angularDebug ? 'settings-fetch-inimage.json' : 'fetch';
                API.get(method, params, done);
            }

            function update(settings, done) {
                var data = "settings=" + encodeURIComponent(JSON.stringify(settings));
                return API.post('update?tlid=' + settings.taglink_id, data, done);
            }

        }
    ]);
