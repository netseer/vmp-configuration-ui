

APP_MODULE
    .controller('PlacementQueueCtrl', [
        '$scope',
        '$state',
        '$http',
        '$location',
				'$timeout',
        'Settings',
				'config',
        function($scope, $state, $http, $location, $timeout, Settings, config){

						$scope.version = config.version;

            $scope.taglink = $location.search().tlid; //$state.params.taglink;

						$scope.renderingTypeEnum = ['Static','InImage','InView','InPage','ThirdParty'];

						$scope.renderingTypeString = "";

            $scope.isNew = !$scope.taglink;

            $scope.taglinkType = 'mobile';

						$scope.showSuccessMsg = false;

            // Default values to use for a new taglink.
            $scope.defaults = {
                enableRefreshing: "",
                enableRescanning: "",
                initialPlacementInViewport: "",
                enableUGC: true,
                maxInitialPlacements: 3,
                minImageHeight: $scope.taglinkType === 'desktop' ? 200 : 125,
                minImageWidth: $scope.taglinkType === 'desktop' ? 400 : 125,
								minTransparency: 0,
								maxTransparency: 100,
                prefetchAds: 3,
                primaryImageSelector: 'img',
                rescanPlacementsMax: 3,
                rescanTimer: 5
            }

						$scope.updateDefaults = function(){

							$scope.defaults.minImageHeight = 0;
							$scope.defaults.minImageWidth = 0;

						}

            // Options for various dropdowns in the UI.
            $scope.options = {
                enableRescanning: [
                    {label:"Disabled",value:null},
                    {label:"Timer",value:"timer"},
                    {label:"Selector",value:"selector"},
                    {label:"Scroll",value:"scroll"},
                    {label:"Swipe",value:"swipe"},
                    {label:"Tag Added",value:"tagdrop"}
                ],
                enableRefreshing: [
                    {label:"Disabled",value:null},
                    {label:"URL Change",value:"url_change"},
                    {label:"Selector",value:"selector"},
                    {label:"Swipe",value:"swipe"},
                    {label:"Tag Added",value:"tagdrop"}
                ]
            };

            $scope.hasError = false;

            $scope.errorSaving = false;

            $scope.isAdvancedHidden = true;

            $scope.requestPending = false;

            $scope.user = {};

            $scope.taglinkData = null;

						$scope.ugcEnabled = false;

						$scope.setUGC = function(taglinkData) {

							$scope.ugcEnabled = taglinkData
																	&& taglinkData.user_generated_content
																	&& taglinkData.user_generated_content.twitter && taglinkData.user_generated_content.twitter.enabled
																	&& taglinkData.user_generated_content.instagram && taglinkData.user_generated_content.instagram.enabled
																	&& taglinkData.user_generated_content.imgur && taglinkData.user_generated_content.imgur.enabled
																	&& taglinkData.user_generated_content.pinterest && taglinkData.user_generated_content.pinterest.enabled;

						}

            if ($scope.isNew) {

                $scope.taglinkData = angular.copy($scope.defaults);

            } else {

								$scope.requestPending = true;

                Settings.fetch({tlid:$scope.taglink}, function(err, data){
                    if (data) {
                        $scope.taglinkData = data;
												$scope.taglinkData.taglink_id = $scope.taglink;
												$scope.setUGC($scope.taglinkData);
												$scope.renderingTypeString = $scope.renderingTypeEnum[$scope.taglinkData.rendering_type];
												$scope.taglinkType = data.delivery_medium ? data.delivery_medium.toLowerCase() : 'mobile';
												$scope.updateDefaults();
                    }

										$scope.requestPending = false;
                });

            }

            $scope.toggleAdvancedSettings = function(){
                if ($scope.isAdvancedHidden===true) {
                    $scope.isAdvancedHidden = false;
                } else {
                    $scope.isAdvancedHidden = true;
                }
            }

            $scope.patterns = {
                integersOnly: '\\d+',
								multiplesOfFive: '0|5|10|15|20|25|30|35|40|45|50|55|60|65|70|75|80|85|90|95|100'
            };

            $scope.save = function(){
                $scope.requestPending = true;
                Settings.update($scope.taglinkData, function(err, data){
                    $scope.requestPending = false;
                    if (data) {
                        $scope.errorSaving = false;
												$scope.settingsForm.$setPristine();
												$scope.showSuccessMsg = true;
												$timeout(function(){
													$scope.showSuccessMsg = false;
												}, 2000);
                    } else {
                        $scope.errorSaving = true;
												$timeout(function(){
													$scope.errorSaving = false;
												}, 2000);
                    }
                });
            }

            //for debugging
            $scope.showFormState = function(){
                console.log($scope.settingsForm.$error);
            }

						$scope.handleGlobalUGC = function(el) {

							$scope.ugcEnabled = el.ugcEnabled;

							if (!$scope.taglinkData || !$scope.taglinkData.user_generated_content) { return; }

							for (var ugcType in $scope.taglinkData.user_generated_content) {
								$scope.taglinkData.user_generated_content[ugcType].enabled = $scope.ugcEnabled;
							}

						}

        }
    ]);