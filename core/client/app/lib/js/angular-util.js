angular.module('angular-util', [])

	.factory('Util', [
		'$rootScope',
		'$state',
		'$window',
		'$q',
		function($rootScope, $state, $window, $q) {

			var requestId = 1;

			return {
				responseHandler: function(promise, done) {
					var self = this;
					var deferred = $q.defer();

					//if (promise) {
					//	promise
					//		.success(function(data) {
					//			if (data.error) {
					//				done && done(data.error.message);
					//				deferred.reject(data.error.message)
					//			}
					//			else if (!data || !data.result) {
					//				done && done('Error with the response.');
					//				deferred.reject(data);
					//			}
					//			else {
					//				done && done(null, data.result);
					//				deferred.resolve(data.result);
					//			}
					//		})
					//		.error(function(data){
					//			self.handleError(data);
					//			done && done(data);
					//			deferred.reject(data);
                    //
					//		});
					//}
					if (promise) {
						promise
							.success(function(data) {
                                if (!data || data.error) {
									done && done('Error with the response:' + data.msg);
									deferred.reject(data);
								}
								else {
									done && done(null, data);
									deferred.resolve(data);
								}
							})
							.error(function(data){
								self.handleError(data);
								done && done(data);
								deferred.reject(data);

							});
					}

					return deferred.promise;
				},

				rpcWrapper: function(method, params) {
					return {
						jsonrpc: '2.0',
						id: this.generateRequestId(),
						method: method,
						params: params || {}
					};
				},

				handleError: function (err) {

					// TODO: Add error message to login page

					switch (err) {
						case 'unauthorized':
							$state.go('public.login');
							break;
						case 'could not identify user.':
							$state.go('public.login');
							break;
						case 'must provide user id':
							$state.go('public.login', {reload: true});
							break;
					}
				},

				generateRequestId: function() {
					return ++requestId + '';
				},

				broadcast: function() {
					$rootScope.$broadcast.apply($rootScope, arguments);
				},

				objectToDate: function(obj) {
					var date = new Date();

					if (obj) {
						date.setDate(obj.day);
						date.setMonth(obj.month);
						date.setFullYear(obj.year);
						date.setHours(0,0,0,0);
					}

					return date;
				}
			};
		}]);
