"use strict";

const nconf = require('nconf');
const env = process.env.NODE_ENV;

var devConfig = require('../env/default.json');
var stagingConfig = require('../env/acceptance.json');
var productionConfig = require('../env/production.json');
var pkg = require('../../../package.json');

module.exports = {
    development: {
			angularDebug: true,
			environment: 'development',
			apiUrl: devConfig.app.apiUrl,
			basePath: '/',
			title: 'Netseer PubHub - Development',
			version: pkg.version
    },
    acceptance: {
			angularDebug: false,
			environment: 'acceptance',
			apiUrl: stagingConfig.app.apiUrl,
			basePath: '/pubhub/angular-app/',
			title: 'Netseer PubHub - Staging',
			version: pkg.version
    },
    production: {
			angularDebug:false,
			environment: 'production',
			apiUrl: productionConfig.app.apiUrl,
			basePath: '/pubhub/angular-app/',
			title: 'Netseer PubHub',
			version: pkg.version
    }
};